FROM fedora:41

RUN dnf -y install \
        git \
        python3-pip \
 && dnf -y update \
 && dnf clean all

RUN sed -i -e 's/# %wheel/%wheel/' -e '0,/%wheel/{s/%wheel/# %wheel/}' /etc/sudoers

ARG HOST_USER_ID=5555
ENV HOST_USER_ID ${HOST_USER_ID}
RUN useradd -u $HOST_USER_ID -G wheel -ms /bin/bash user

USER user
WORKDIR /home/user

ENV LANG C.UTF-8
ENV PATH /home/user/.local/bin:/usr/bin
