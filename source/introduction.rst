Platform Introduction
=====================

The GNOME platform consists of libraries, services and tools which provide everything you need to create and distribute high-quality apps. The platform covers everything from basic UI creation, code editing and building, and app distribution, through to more specialised libraries and services.

This introduction provides an overview of the GNOME platform. It is a great way to get started for those who are new to the GNOME platform, as well as being a valuable reference for those who are looking for particular specialist functionality.

The platform introduction is composed of the following sections:

* :doc:`Components <introduction/components>`: an overview of GNOME platform libraries and services
* :doc:`Languages <introduction/languages>`: information on the programming languages that can be used with the GNOME platform
* :doc:`Builder <introduction/builder>`: an introduction to Builder, the GNOME IDE
* :doc:`Flatpak <introduction/flatpak>`: an introduction to Flatpak, GNOME's preferred app distribution technology

.. toctree::
   :maxdepth: 1
   :hidden:

   introduction/components.rst
   introduction/languages.rst
   introduction/builder.rst
   introduction/flatpak.rst
   GNOME Components <https://developer.gnome.org/components/>
