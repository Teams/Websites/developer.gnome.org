Platform Components
===================

This part of the GNOME platform introduction provides a comprehensive overview 
of the libraries and services that are provided by the GNOME project, and which 
provide the primary basis for creating apps using the GNOME platform.

:doc:`GNOME libraries <overview/libraries>` include things like GTK, for
building application user interfaces, GStreamer, for multimedia playback, and
GSocket networking APIs. These are available to use through the GNOME Flatpak
runtime, as well as through the main Linux distributions.

:doc:`GNOME services <overview/services>` are included in the system and give 
apps access to things like email, calendaring, contacts, and password storage. 
One of the most useful services for apps is portals, which provides sandboxed 
apps with access to a wide range of system features.

.. toctree::
   :maxdepth: 1
   :hidden:

   overview/libraries.rst
   overview/services.rst
